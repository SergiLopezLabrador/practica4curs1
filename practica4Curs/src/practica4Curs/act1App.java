package practica4Curs;

public class act1App {

	public static void main(String[] args) {
		
		//Aqu� crear� variables amb valor escollit per mi
		
		int NUMERO1 = 9;
		int NUMERO2 = 23;
		
		//Aqu� far� les operacions del exercici i ho mostrar� per consola
		System.out.println("Suma entre numero1 i numero2: " + (NUMERO1+NUMERO2));
		System.out.println("Resta entre numero1 i numero2: " + (NUMERO1-NUMERO2));
		System.out.println("Multiplicaci� entre numero1 i numero2: " + (NUMERO1*NUMERO2));
		System.out.println("Divisi� entre numero1 i numero2: " + (NUMERO1/NUMERO2));
		System.out.println("M�dul entre numero1 i numero2: " + (NUMERO1%NUMERO2));

	}

}
